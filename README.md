# IconPython
This repo scripts can generate icon/favicon with Pillow from specific folders. It's work with Pythons [3.7, 3.9].

# How it's work
To use it, launch the script "generate.py" ; before add your PNG file.s inside a "source" folder (`%ROOT%/source`) ; 
when your script is done, all files has generated inside the `generate` folder (`%ROOT%/generate`).

> One configuration are available with Pycharm to quickly launch the icon/favicon generate.
