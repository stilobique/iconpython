from PIL import Image
from os.path import join, abspath, dirname, isfile
from os import walk
from pathlib import Path


def make_ico(to_do, to_export):
    try:
        item_done = []
        for root, dirs, files in walk(to_do):
            for file in files:
                file = join(root, file)
                if ".keep" in file:
                    continue
                if isfile(file):
                    export = Image.open(file)
                    icon_name_windows = Path(file).stem + '.ico'
                    item_done.append(icon_name_windows)
                    export.save(join(to_export, icon_name_windows))

        return item_done
    except FileNotFoundError:
        return print("No file found")


if __name__ == "__main__":
    path = dirname(abspath(__file__))
    path_source_ico = join(path, 'icon/source')
    path_output_ico = join(path, 'icon/generate')
    path_source_fav = join(path, 'favicon/source')
    path_output_fav = join(path, 'favicon/generate')

    icon = make_ico(path_source_ico, path_output_ico)
    print("Convert Ico. Make {0} icon(s).".format(len(icon)))

    favicon = make_ico(path_source_fav, path_output_fav)
    print("Convert all Favicon. Make {0} favicon(s).".format(len(favicon)))
